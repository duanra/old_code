%Cardioide
a=input('Entrer la constante a = ');
x=(0:0.1:4*pi);
y=(a/2)*(1+cos(x));
figure(1);
subplot(1,2,1),polar(x,y);
title('Courbe d une cardoide'); grid on;
subplot(1,2,2),plot(x,y);
xlabel('abscisses'); ylabel('ordonn�es');
